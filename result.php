<html lang="fr">
<head>
    <title>Questionnaire FAPO</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Questionnaire FAPO</title>
    <link rel="stylesheet" type="text/css" href="./bootstrap.css">
    <link rel="stylesheet" type="text/css" href="./slick.css">
    <link rel="stylesheet" type="text/css" href="./style.css">
    <link rel="stylesheet" type="text/css" href="./carousel.css">
    <script src="./jquery-3.4.1.min.js"></script>
    <script src="./bootstrap.js"></script>
    <script src="./slick.js"></script>
    <style type="text/css">

        p { margin-left: 80px; margin-right: 80px; margin-bottom: 0.25cm; line-height: 115%; background: transparent }
        h3 { alignmargin-left: 80px; margin-top: 0.25cm; margin-bottom: 0.21cm; background: transparent; page-break-after: avoid }
        h3.western { font-family: "Liberation Sans", sans-serif; font-size: 14pt; font-weight: bold }
        h3.cjk { font-family: "Noto Sans CJK SC"; font-size: 14pt; font-weight: bold }
        h3.ctl { font-family: "Lohit Devanagari"; font-size: 14pt; font-weight: bold }
        h1 { margin-bottom: 0.21cm; background: transparent; page-break-after: avoid }
        h1.western { font-family: "Liberation Sans", sans-serif; font-size: 18pt; font-weight: bold }
        h1.cjk { font-family: "Noto Sans CJK SC"; font-size: 18pt; font-weight: bold }
        h1.ctl { font-family: "Lohit Devanagari"; font-size: 18pt; font-weight: bold }
        a:link { color: #000080; so-language: zxx; text-decoration: underline }
        a:visited { color: #800000; so-language: zxx; text-decoration: underline }
    </style>

</head>
<?php
function replace_semi_coma($value_string) {
    return str_replace(";", ",", $value_string);
}
if (isset($_POST)) {
    $lever_lines = '';
    $brake_lines = '';
    $activity_other = '';

    foreach ($_POST as $inputName => $inputValue) {
        switch ($inputName) {
            case 'activity':
                $activity = replace_semi_coma($inputValue);

            case 'activity_other':
                $activity_other = replace_semi_coma($inputValue);
            case 'firstName':
                $firstName = replace_semi_coma($inputValue);

            case 'lastName':
                $lastName = replace_semi_coma($inputValue);

            case 'email':
                $email = replace_semi_coma($inputValue);
                print_r($email);
                print_r(" coucou ");


        }
        if (empty($activity_other)) {
            $activity_other = $activity;
        }
        if (substr($inputName, 0, 5) === "lever") {
            $lever_lines = htmlspecialchars($lever_lines.replace_semi_coma($inputValue)."\n");
        }
        if (substr($inputName, 0, 5) === "brake") {
            $brake_lines = $brake_lines.replace_semi_coma($inputValue)."\n";
        }
    }
    if(isset($_POST['send_results']) &&
        $_POST['send_results'] == 'Yes')
    {
        fwrite(fopen("./results/send_results.txt", 'a'), "$firstName $lastName <$email>\n");
    }
    if(isset($_POST['send_future']) &&
        $_POST['send_future'] == 'Yes')
    {
        fwrite(fopen("./results/send_future.txt", 'a'), "$firstName $lastName <$email>\n");
    }

    fwrite(fopen("./results/people.txt", 'a'), "$firstName;$lastName;$email;$activity_other\n");
    fwrite(fopen("./results/lever.txt", 'a'),$lever_lines);
    fwrite(fopen("./results/brake.txt", 'a'),$brake_lines);

}


?>
<body>
<!--<div class="container_carousel">-->
<!--    <div class="carousel">-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--        <div class="carousel__face"></div>-->
<!--    </div>-->
<!--</div>-->
<?php
print_r(htmlspecialchars($_POST));
?>
<div>
    <br><br>
<h3 align="center">Merci d'avoir participé à ce questionnaire !</h3>
<br><br><br><br>
    <div class="row">
        <div class="container">
            <section class="customer-logos slider" data-arrows="true">
                <div class="slide"><img src="./logos/Accueil_Paysan.png"></div>
                <div class="slide"><img src="./logos/ADEAR_Occ_jaune.png"></div>
                <div class="slide"><img src="./logos/Agropolis.png"></div>
                <div class="slide"><img src="./logos/BP_OCcitanie.png"></div>
                <div class="slide"><img src="./logos/carasso.png"></div>
                <div class="slide"><img src="./logos/FR_CIVAM_jaune.png"></div>
                <div class="slide"><img src="./logos/Inpact_Occ.png"></div>
                <div class="slide"><img src="./logos/logo-cirad.png"></div>
                <div class="slide"><img src="./logos/Logo_nature_progres.png"></div>
                <div class="slide"><img src="./logos/Marche_paysans.png"></div>
                <div class="slide"><img src="./logos/MP11.png"></div>
                <div class="slide"><img src="./logos/Reneta.png"></div>
                <div class="slide"><img src="./logos/Solagro.png"></div>
                <div class="slide"><img src="./logos/Solidarite_paysans_Occ.png"></div>
                <div class="slide"><img src="./logos/TdL.png"></div>
            </section>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.customer-logos').slick({
                variableWidth: true,
                slidesToShow: 6,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 1000,
                arrows: true,
                dots: false,
                pauseOnHover: false,
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                }, {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 3
                    }
                }]
            });
        });
    </script>
</body>
</html>